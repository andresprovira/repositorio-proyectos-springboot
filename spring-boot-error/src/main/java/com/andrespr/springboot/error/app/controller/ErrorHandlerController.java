package com.andrespr.springboot.error.app.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.andrespr.springboot.error.app.errors.UsuarioNoEncontradoException;

//No se anota con @Controller sino con @ControllerAdvice para manejo de excepciones
//La diferencia con un controlador normal es que en el normal se manejan rutas en los handlers, aqui se maneja una excepción
@ControllerAdvice
public class ErrorHandlerController {

	//En ves de RequestMapping, getMapping... se utiliza @ExceptionHandler() para manejo de excepciones
	@ExceptionHandler(ArithmeticException.class) //Siempre es Excepcion.class
	public String aritmeticaError(Exception e, Model model) {
		model.addAttribute("error","error de aritmética");
		model.addAttribute("message",e.getMessage());
		model.addAttribute("status",HttpStatus.INTERNAL_SERVER_ERROR.value());//error 500
		model.addAttribute("timestamp",new Date());
		return "error/aritmetica";
	}
	
	@ExceptionHandler(NumberFormatException.class)
	public String numberFormatException(Exception e, Model model) {
		model.addAttribute("error","Error:Formato número inválido!");
		model.addAttribute("message",e.getMessage());
		model.addAttribute("status",HttpStatus.INTERNAL_SERVER_ERROR.value());//error 500
		model.addAttribute("timestamp",new Date());
		return "error/numero-formato"; //Pueden ir los dos errores en una misma vista
	}
	
	@ExceptionHandler(UsuarioNoEncontradoException.class)
	public String usuarioNoEncontradoException(Exception e, Model model) {
		model.addAttribute("error","Error:Usuario no encontrado!");
		model.addAttribute("message",e.getMessage());
		model.addAttribute("status",HttpStatus.INTERNAL_SERVER_ERROR.value());//error 500
		model.addAttribute("timestamp",new Date());
		return "error/aritmetica"; //Pueden ir los dos errores en una misma vista(en este caso usaré aritmetica como genérica)
	}
}
