package com.andrespr.springboot.error.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.andrespr.springboot.error.app.errors.UsuarioNoEncontradoException;
import com.andrespr.springboot.error.app.models.Usuario;
import com.andrespr.springboot.error.app.services.UsuarioService;

@Controller
public class AppController {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/index")
	public String index() {
		//Integer valor=100/0; con esto provocamos una excepcion de tipo zero
		//Es importante que las vistas de los errores personalizados estén en una carpeta llamado error
		Integer valor = Integer.parseInt("10x");//error NumberFormatException
		return "index";
	}
	
	@GetMapping("/ver/{id}")
	public String ver(@PathVariable Integer id, Model model) {
		//Usuario usu = usuarioService.obtenerPorId(id);
		//if(usu==null) {
			//throw new UsuarioNoEncontradoException(id.toString());
		//}
		//Otra forma en vez de hacer lo de arriba con la Clase Optional
		Usuario usu = usuarioService.obtenerPorIdOptional(id).orElseThrow(()-> new UsuarioNoEncontradoException(id.toString()));
		model.addAttribute("usuario",usu);
		model.addAttribute("titulo","Detalle usuario: " .concat(usu.getNombre()));
		return "ver";
	}
}
