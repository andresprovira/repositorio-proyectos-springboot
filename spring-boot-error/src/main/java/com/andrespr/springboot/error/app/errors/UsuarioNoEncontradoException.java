package com.andrespr.springboot.error.app.errors;

//Creando mis propias excepciones para personalizarlas
public class UsuarioNoEncontradoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNoEncontradoException(String id) {
		super("Usuario con id: ".concat(id).concat(" no encontrado en el sistema") );
		
	}

	
}
