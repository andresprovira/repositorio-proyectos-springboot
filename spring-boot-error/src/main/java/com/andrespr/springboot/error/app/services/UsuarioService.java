package com.andrespr.springboot.error.app.services;

import java.util.List;
import java.util.Optional;

import com.andrespr.springboot.error.app.models.Usuario;

public interface UsuarioService {

	public List<Usuario> listar();
	public Usuario obtenerPorId(Integer id);
	public Optional<Usuario> obtenerPorIdOptional(Integer id); //Optional, api de java 8, ayuda con errores y diferentes caminos
		
	
}
