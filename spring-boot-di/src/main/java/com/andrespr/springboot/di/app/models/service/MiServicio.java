package com.andrespr.springboot.di.app.models.service;

import org.springframework.stereotype.Service;

//Se puede poner nombre a los componentes
@Service("miServicioSimple")
public class MiServicio implements IServicio{

	@Override
	public String operacion() {
		
		return "Esto es una operacion con autowired con simple";
	}

	
}
