package com.andrespr.springboot.di.app;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.andrespr.springboot.di.app.models.domain.ItemFactura;
import com.andrespr.springboot.di.app.models.domain.Producto;

@Configuration
public class AppConfig {
	//Sirve mucho para APIS
	/*Esta clase de configuracion sirve para crear los beans aparte, sin necesidad de usar anotaciones como service o component*/
//	@Primary
//	@Bean("miServicioSimple")
//	public IServicio registrarMiServicio() {
//		return new MiServicio();
//	}
//	
//	@Bean("miServicioComplejo")
//	public IServicio registrarMiServicioComplejo() {
//		return new MiServicioComplejo();
//	}
	
	@Bean("itemsFactura")
	public List<ItemFactura> registrarItems(){
		
		Producto producto1 = new Producto("Camara Sony", 100);
		Producto producto2 = new Producto("bicicleta", 200);
		
		ItemFactura linea1 = new ItemFactura(producto1,2);
		ItemFactura linea2 = new ItemFactura(producto2,4);
		
		List<ItemFactura> factura = new ArrayList<>();
		factura.add(linea1);
		factura.add(linea2);
		
		return factura;
	}
	
	@Bean("itemsFacturaOficina")
	public List<ItemFactura> registrarItemsOficina(){
		
		Producto producto1 = new Producto("Monitor LG", 250);
		Producto producto2 = new Producto("Notebook Asus", 500);
		Producto producto3 = new Producto("Impresora HP", 80);
		Producto producto4 = new Producto("Escritorio oficina",300);
		
		ItemFactura linea1 = new ItemFactura(producto1,2);
		ItemFactura linea2 = new ItemFactura(producto2,1);
		ItemFactura linea3 = new ItemFactura(producto3,1);
		ItemFactura linea4 = new ItemFactura(producto4,1);
		
		List<ItemFactura> factura = new ArrayList<>();
		factura.add(linea1);
		factura.add(linea2);
		factura.add(linea3);
		factura.add(linea4);
		
		return factura;
	}
}
