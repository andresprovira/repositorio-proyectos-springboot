package com.andrespr.springboot.di.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.andrespr.springboot.di.app.models.service.IServicio;

@Controller
public class IndexController {

	//Tres formas de injectar dependencias con autowired
	
	//1. A un atributo del servicio
	@Autowired
	//Con Qualifier indicamos que servicio queremos implementar, en este caso mi servicio simple
	//@Qualifier("miServicioSimple")
	private IServicio servicio;
	
	
	//2. Como parámetro del constructor, no necesita de la etiqueta @Autowired
	/*public IndexController(MiServicio servicio) {
		super();
		this.servicio = servicio;
	}*/



	@GetMapping({"/index", "/", ""})
	public String index(Model model) {
		model.addAttribute("objeto", servicio.operacion());
		return "index";
	}
	
	
	/*
	 3.En el método set de mi servicio
	 
	 @Autowired
	  public void setServicio(MiServicio servicio) {
		this.servicio = servicio;
	}
	*/
	
}
