package com.andrespr.springboot.di.app.models.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
//La etiqueta @Primary sirve para decirle a un atributo que se ha aplicado una injección de dependencía, cuando existe varias clases con
//esa implementación, cual clase debe usar.
@Primary
public class MiServicioComplejo implements IServicio {

	@Override
	public String operacion() {
		// TODO Auto-generated method stub
		return "ejecutando algún proceso importante complicado....";
	}

	
}
