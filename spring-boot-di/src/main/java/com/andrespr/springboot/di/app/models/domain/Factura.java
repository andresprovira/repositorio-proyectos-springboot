package com.andrespr.springboot.di.app.models.domain;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Component
//Con esta anotacion indicamos que este bean va a durar solo una petición http de usuario
@RequestScope
//El componente dura lo que dure la sesion, se necesita seriarizable, para pasar el componente a bytes para guardarlo en sesion
//@SessionScope
//otra anotacion singleton
//@ApplicationScope
public class Factura implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6461003686876298735L;
	
	
	@Value("${factura.description}")
	private String description;
	@Autowired
	private Cliente cliente;
	@Autowired
	@Qualifier("itemsFacturaOficina")
	private List<ItemFactura> items;
	
	//Se ejecuta justo despues de generar el objeto y de haber inyectado la dependencia
	@PostConstruct
	public void inicializar() {
		cliente.setNombre(cliente.getNombre().concat(" ").concat("José"));
		description = description.concat(" del Cliente: ").concat(cliente.getNombre());
	}
	//Se ejecuta antes de eliminar el objeto, como es singleton se ejecutará cuando se termine la aplicación
	//singleton: se mantiene en el contenedor de spring una sola instancia de un componente hasta que finaliza la aplicacion
	@PreDestroy
	public void destruir() {
		System.out.println("Factura destruida: ".concat(description));
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<ItemFactura> getItems() {
		return items;
	}
	public void setItems(List<ItemFactura> items) {
		this.items = items;
	} 
	
	
}
