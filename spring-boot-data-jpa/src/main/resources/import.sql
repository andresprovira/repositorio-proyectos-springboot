INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Andres', 'Jimenez', 'jandrescamilo199@gmail.com', '2022-08-28', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES(  'Claudia', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Maria', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Miguel', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Jose', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Alberto', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Emilio', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Adri', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES('Manu', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Jhon', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Pou', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Melani', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Pepa', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Pepe', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Paquito', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Diego', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Ana', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Isa', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Nose', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');
INSERT INTO clientes ( nombre, apellido, email, create_at, foto) VALUES( 'Ultimo', 'Rodriguez', 'clohel@gmail.com', '2022-04-21', '');


/* Tabla productos*/

INSERT INTO productos (nombre, precio, create_at) VALUES('Panasonic Pantalla LCD', 149, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Movil xiaomi', 99, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Ordenador Gamin', 699, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Teclado', 49, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Raton', 29, NOW());

/* Tabla facturas*/

INSERT INTO facturas (descripcion, observacion, cliente_id ,create_at) VALUES('Factura 1', null, 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 3);

INSERT INTO facturas (descripcion, observacion, cliente_id ,create_at) VALUES('Factura 2', 'Esto es un ejemplo de observaciones', 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(3, 2, 5);

/* Tabla Roles*/
INSERT INTO roles (id,nombre_rol) VALUES(1,'ROLE_USER');
INSERT INTO roles (id,nombre_rol) VALUES(2,'ROLE_ADMIN');
/*tabla usuarios*/



INSERT INTO users (username,password,enabled,role_id) VALUES('andres','$2a$10$LBsUkkj3v1PrhN/CHrXUAe.3hp1Ae4E3H4fBPBIHO9PkYzWZ9rjg2',1,1);
INSERT INTO users (username,password,enabled,role_id) VALUES('admin','$2a$10$UfV4//ynq.o8lEdE9FK4zeG8GzN9KnbjCe0WEnehkCQ2RbAOUHICq',1,2);

