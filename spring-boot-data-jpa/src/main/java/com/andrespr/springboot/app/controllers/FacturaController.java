package com.andrespr.springboot.app.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.andrespr.springboot.app.models.Cliente;
import com.andrespr.springboot.app.models.Factura;
import com.andrespr.springboot.app.models.ItemFactura;
import com.andrespr.springboot.app.models.Producto;
import com.andrespr.springboot.app.services.IClienteService;

@Secured("ROLE_ADMIN") //También se puede usar encima del controlador para que se aplique a todos los métodos
@Controller
@RequestMapping("/factura")
@SessionAttributes("factura")
public class FacturaController {

	@Autowired
	private IClienteService clienteService;
	

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	//CREATE
	@GetMapping("/form/{clienteId}")
	public String crear(@PathVariable(value = "clienteId") Long clienteId, Model model, RedirectAttributes flash) {
		
		Cliente cliente = clienteService.findOne(clienteId);
		
		if(cliente==null) {
			
			flash.addFlashAttribute("error","el cliente no existe en la base de datos");
			return "redirect:/listar";
		}
		
		Factura factura= new Factura();
		factura.setCliente(cliente);
		
		model.addAttribute("titulo","Crear Factura");
		model.addAttribute("factura",factura);
		return "factura/form";
	}
	
	@GetMapping(path = "/cargar-productos/{term}",produces = {"application/json"} )
	public @ResponseBody List<Producto> cargarProductos(@PathVariable String term){
		return clienteService.findByNombre(term);
	}
	
	//SAVE
	@PostMapping(value = "/form")
	public String guardar(@Valid Factura factura,
			BindingResult result,
			Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemID,
			@RequestParam(name = "cantidad[]", required = false) Integer[] cantidad,
			RedirectAttributes flash,
			SessionStatus status){
		
			if(result.hasErrors()) {
				model.addAttribute("titulo","Crear Factura");
				return "factura/form";
			}
		
			if(itemID==null || itemID.length==0) {
				model.addAttribute("titulo","Crear Factura");
				model.addAttribute("error","Error: la factura NO puede no tener líneas");
				return "factura/form";
			}
		
		for(int i=0; i< itemID.length;i++) {
			Producto producto = clienteService.findProductoById(itemID[i]);
			
			ItemFactura linea = new ItemFactura();
			linea.setCantidad(cantidad[i]);
			linea.setProducto(producto);
			factura.addItemsFactura(linea);
			
			log.info("ID: " + itemID[i].toString() + ", cantidad: " + cantidad[i]);
		}
		
		clienteService.saveFactura(factura);
		status.setComplete();
		
		flash.addFlashAttribute("success","factura creada con éxito");
		return "redirect:/ver/" + factura.getCliente().getId();
	}
	
	//Detalle
	@GetMapping(value = "/ver/{facturaId}")
	public String verFactura(@PathVariable Long facturaId,
			Model model,
			RedirectAttributes flash) {
		
		Factura factura = clienteService.findFacturaById(facturaId);//clienteService.fetchFacturaByIdWithClienteWithItemFacuraWithProducto(facturaId);//
		
		if(factura==null) {
			flash.addFlashAttribute("error", "La factura no existe en la base de datos");
			return "redirect:/lista";
		}
		
		model.addAttribute("factura",factura);
		model.addAttribute("titulo", "Factura: ".concat(factura.getDescripcion()));
		
		return "/factura/ver";	
	}
	
	@GetMapping("/eliminar/{facturaId}")
	public String eliminar(@PathVariable Long facturaId, RedirectAttributes flash) {
		Factura factura = clienteService.findFacturaById(facturaId);
		
		if(factura!=null) {
			clienteService.deleteFactura(facturaId);
			flash.addFlashAttribute("success", "Factura Eliminada con éxito");
			return "redirect:/ver/" + factura.getCliente().getId();
		}
		flash.addFlashAttribute("error", "La factura no existe en la base de datos, no se pudo eliminar");
		return "redirect:/lista";
	}
}
