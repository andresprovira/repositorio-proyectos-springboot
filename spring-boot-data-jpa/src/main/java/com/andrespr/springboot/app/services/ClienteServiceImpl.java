package com.andrespr.springboot.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.andrespr.springboot.app.models.Cliente;
import com.andrespr.springboot.app.models.Factura;
import com.andrespr.springboot.app.models.Producto;
import com.andrespr.springboot.app.models.dao.IClienteDao;
import com.andrespr.springboot.app.models.dao.IFacturaDao;
import com.andrespr.springboot.app.models.dao.IProductoDao;

@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteDao clienteDao;
	
	@Autowired
	private IProductoDao productoDao;
	
	@Autowired
	private IFacturaDao facturaDao;

	@Transactional(readOnly = true)
	@Override
	public List<Cliente> findAll() {

		return (List<Cliente>) clienteDao.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Cliente findOne(Long id) {

		return clienteDao.findById(id).orElse(null);//o tambien .orElse(null) o .get()
	}

	@Transactional
	@Override
	public void save(Cliente cliente) {
		clienteDao.save(cliente);

	}

	@Transactional
	@Override
	public void delete(Long id) {

		clienteDao.deleteById(id);
	}

	@Transactional(readOnly = true)
	@Override
	public Page<Cliente> findAll(Pageable pageable) {
		
		return clienteDao.findAll(pageable);
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<Producto> findByNombre(String nombre) {
		
		//return productoDao.findByNombreLikeIgnoreCase("%"+ nombre + "%"); con la otra forma
		return productoDao.findByNombre(nombre);
	}

	@Transactional
	@Override
	public void saveFactura(Factura factura) {
		
		facturaDao.save(factura);
		
	}
	@Transactional(readOnly = true)
	@Override
	public Producto findProductoById(Long id) {
		
		return productoDao.findById(id).orElse(null);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Factura findFacturaById(Long id) {
		
		return facturaDao.findById(id).orElse(null);
	}

	@Transactional
	@Override
	public void deleteFactura(Long id) {
		
		facturaDao.deleteById(id);
		
	}

	/*@Transactional(readOnly = true)
	@Override
	public Factura fetchFacturaByIdWithClienteWithItemFacuraWithProducto(Long id) {
		// TODO Auto-generated method stub
		return facturaDao.(id);
	}
*/
}
