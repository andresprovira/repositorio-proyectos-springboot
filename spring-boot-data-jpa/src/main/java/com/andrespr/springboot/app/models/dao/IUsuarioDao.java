package com.andrespr.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.andrespr.springboot.app.models.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

	//No es necesario hacer la query, lo hace a través del nombre del método  https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
	public Usuario findByUsername(String username);
}
