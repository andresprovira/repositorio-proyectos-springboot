package com.andrespr.springboot.app.services;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;//Importante debe ser esta importación

import com.andrespr.springboot.app.models.Cliente;
import com.andrespr.springboot.app.models.Factura;
import com.andrespr.springboot.app.models.Producto;

public interface IClienteService {

	public List<Cliente> findAll();
	public Page<Cliente> findAll(Pageable pageable);
	public void save(Cliente cliente);
	public Cliente findOne(Long id);
	public void delete(Long id);
	
	public List<Producto> findByNombre(String nombre);
	public void saveFactura(Factura factura);
	
	public Producto findProductoById(Long id);
	
	public Factura findFacturaById(Long id);
	
	public void deleteFactura(Long id);
	
	//public Factura fetchFacturaByIdWithClienteWithItemFacuraWithProducto(Long id);
}
