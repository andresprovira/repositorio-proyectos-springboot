package com.andrespr.springboot.app.models.dao;




import org.springframework.data.repository.PagingAndSortingRepository;

import com.andrespr.springboot.app.models.Cliente;

//No es necesario la anotacion @Repository, ya que hereda de CrudRepository que ya es un componente de Spring
public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long> {

	
}
