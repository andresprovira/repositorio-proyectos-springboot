package com.andrespr.springboot.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.andrespr.springboot.app.models.Factura;

public interface IFacturaDao extends CrudRepository<Factura, Long> {

	//@Query("select f from factura f where f join fetch f.cliente c join fetch f.items l join fetch l.producto where f.id=?1")
	//public Factura fetchByIdWithClienteWithItemFacuraWithProducto(Long id);
}
