package com.andrespr.springboot.app.controllers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.andrespr.springboot.app.models.Cliente;
import com.andrespr.springboot.app.services.IClienteService;
import com.andrespr.springboot.app.services.IUploadFileService;
import com.andrespr.springboot.app.util.paginator.PageRender;

@Controller
@SessionAttributes("cliente") // Cada vez que se invoca el crear o editar, se guarda en sesion el cliente
								// Es mejor usar SessionAttributes que hidden id en un formulario
public class ClienteController {

	protected final Log logger= LogFactory.getLog(this.getClass());
	@Autowired
	private IClienteService clienteService;

	@Autowired
	private IUploadFileService uploadService;

	// FORMA 2 DE SUBIR UNa IMAGEN, NO ME GUSTA
	@Secured({"ROLE_USER", "ROLE_ADMIN"}) //anotación que sirve para no tener que ponerlo en el archivo springSecurity
											//con {} podemos poner varios Roles
	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename) {

		Resource recurso = uploadService.load(filename);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}

	// Detalle
	//Otra forma
	//@PreAuthorize("hasRole('ROLE_USER')")
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	@GetMapping(value = "/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {

		Cliente cliente = clienteService.findOne(id);
		if (cliente == null) {
			flash.addFlashAttribute("error", "El cliente no existe en la base de datos");
			return "redirect:/listar";
		}
		model.addAttribute("cliente", cliente);
		model.addAttribute("titulo", "Detalle cliente: " + cliente.getNombre());

		return "ver";
	}

	// Listar
	@RequestMapping(value = { "/listar", "/" }, method = RequestMethod.GET)
	public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model,
			Authentication authentication,
			HttpServletRequest request) {

		if(authentication!=null) {
			logger.info("Hola usuario autenticado, tu username es: " + authentication.getName());
		}
		 
		/*if(hasRole("ROLE_ADMIN", authentication)) {
			logger.info("Hola " + authentication.getName() + " tienes acceso");
		}else {
			logger.info("Hola " + authentication.getName() + " No tienes acceso");
		}*/
		
		/*SecurityContextHolderAwareRequestWrapper securityContext = new SecurityContextHolderAwareRequestWrapper(request, "");
		if(securityContext.isUserInRole("ROLE_ADMIN")) {
			logger.info("Hola " + authentication.getName() + " tienes acceso");
		}else {
			logger.info("Hola " + authentication.getName() + " No tienes acceso");
		}*/
		
		//Forma estática de manejar authentication
		//Authentication auth= SecurityContextHolder.getContext().getAuthentication();
		//if(auth!=null) {
		//	logger.info("  SecurityContextHolder.getContext().getAuthentication() :Hola usuario autenticado, tu username es: " + authentication.getName());
		//}
		Pageable pageRequest = PageRequest.of(page, 5);// cinco registros por página
		Page<Cliente> clientes = clienteService.findAll(pageRequest);

		PageRender<Cliente> pageRender = new PageRender<>("/listar", clientes);// url y listado de clientes
		model.addAttribute("titulo", "Listado de clientes");
		model.addAttribute("listClientes", clientes);
		model.addAttribute("page", pageRender);
		return "listar";
	}

	// Create
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form")
	public String crear(Model model) {

		Cliente cliente = new Cliente();
		model.addAttribute("titulo", "Formulario de cliente");
		model.addAttribute("cliente", cliente);
		return "form";
	}

	// Save
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form", method = RequestMethod.POST) // Para crear los flash, es como un model
	public String guardar(@Valid Cliente cliente, BindingResult binding, Model model,
			@RequestParam("file") MultipartFile foto, RedirectAttributes flash, SessionStatus status) {

		if (binding.hasErrors()) {
			model.addAttribute("titulo", "Formulario de cliente");
			return "form";
		}

		if (!foto.isEmpty()) {
			// Aqui se van a guardar nuestras imagenes, lo suyo es que sea en un directorio
			// fuera del proyecto en produccion
			// String rootPath= "D://Temp//uploads"; ruta fuera del proyecto

			if (cliente.getId() != null && cliente.getId() > 0 && cliente.getFoto() != null
					&& cliente.getFoto().length() > 0) {
				// Pasos para poder cambiar una foto cuando editamos un cliente. resolve("nombre
				// del archivo")
				uploadService.delete(cliente.getFoto());

			}
			String uniqueFilename = uploadService.copy(foto);
			flash.addFlashAttribute("info", "Has subido correctamente " + uniqueFilename);

			cliente.setFoto(uniqueFilename);
		}
		// 2 mensajes segun si la id del cliente es null
		String mensajeFlash = (cliente.getId() != null) ? "Cliente editado con éxito" : "Cliente creado con éxito";

		clienteService.save(cliente);
		status.setComplete();// eliminamos objeto cliente de la sesion
		flash.addFlashAttribute("success", mensajeFlash);
		return "redirect:/listar";
	}

	// Editar
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form/{id}")
	public String editar(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Cliente cliente = null;
		if (id > 0) {
			cliente = clienteService.findOne(id);
			if (cliente == null) {
				flash.addFlashAttribute("error", "El ID del cliente no existe en la base de datos");
				return "redirect:/listar";
			}
		} else {
			flash.addFlashAttribute("error", "El ID del cliente no puede ser cero");
			return "redirect:/listar";
		}

		model.addAttribute("titulo", "Editar Cliente");
		model.addAttribute("cliente", cliente);
		return "form";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {

		if (id > 0) {
			Cliente cliente = clienteService.findOne(id);

			clienteService.delete(id);
			flash.addFlashAttribute("success", "cliente eliminado con éxito");
			// Pasos para poder eliminar una foto cuando borramos un cliente.
			// resolve("nombre del archivo")

			if (uploadService.delete(cliente.getFoto()))
				flash.addFlashAttribute("info", "Foto: " + cliente.getFoto() + " eliminada con éxito");

		}
		return "redirect:/listar";
	}
	
	private boolean hasRole(String role,Authentication authentication) {
		
		if(authentication ==null) {
			return false;
		}
		
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		
		//Otra forma sin iterar
		//return authorities.contains(new SimpleGrantedAuthority(role));
		
		for(GrantedAuthority authority:authorities) {
			if(role.equals(authority.getAuthority())) {
				logger.info("Hola " + authentication.getName() + "Tu rol es: " +authority.getAuthority() + " tienes acceso");
				return true;
			}
		}
		return false;
	}
}
