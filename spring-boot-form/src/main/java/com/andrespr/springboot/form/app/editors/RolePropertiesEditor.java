package com.andrespr.springboot.form.app.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.andrespr.springboot.form.app.services.RoleService;

@Component
public class RolePropertiesEditor extends PropertyEditorSupport{
	
	@Autowired
	private RoleService roleService;
	@Override
	public void setAsText(String idString) throws IllegalArgumentException {
		
		
			try {
					Integer id = Integer.parseInt(idString);
		//Devolvemos el pais por la id
						this.setValue(roleService.obtenerPorId(id));
			}catch(NumberFormatException e){
				setValue(null);
			}
		
	}

	
}
