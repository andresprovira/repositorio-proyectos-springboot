package com.andrespr.springboot.form.app.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
//Luego creamos la clase validadora donde queremos añadir esa anotacion, que debe implementar de ConstraintValidator
																		//Anotacion nueva, tipo de dato de atributo al que queremos anotar
public class IdentificaodrRegexValidador implements ConstraintValidator<IdentificadorRegex, String>{

	//Método autogenerado
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		if(value.matches("[0-9]{2}[.][0-9]{3}[.][0-9]{3}[-][A-Z]{1}")) {
			return true;
		}
		return false;
	}

}
