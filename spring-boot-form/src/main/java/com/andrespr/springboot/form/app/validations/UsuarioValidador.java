package com.andrespr.springboot.form.app.validations;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.andrespr.springboot.form.app.models.Usuario;

//Clase para validar los atributos de una clase pojo
//Debe implementar la interfaz Validator de Spring boot
@Component
public class UsuarioValidador implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		//Aquí indicamos la clase que queremos validar. class.isAssgnableFrom(clazz)
		return Usuario.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		//El target en el Objeto a validar, hay que parsearlo a la clase que queremos
		//Usuario usu = (Usuario)target;
									// errors, atributo que queremos validad, mensaje que se encuentra en messages.properties
		//ValidationUtils.rejectIfEmpty(errors, "nombre", "NotEmpty.usuario.nombre");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "NotEmpty.usuario.nombre");
		
		//Otra forma de hacerlo con un if
		/*
		if(usu.getNombre().isEmpty()) {
			errors.rejectValue("nombre", "NotEmpty.usuario.nombre");
		}*/
		
		//matches es un metodo para saber si es igual a un patron
//		if(!usu.getId().matches("[0-9]{2}[.][0-9]{3}[.][0-9]{3}[-][A-Z]{1}")) {
//			errors.rejectValue("id", "Pattern.usuario.id");
//		}

	}

}
