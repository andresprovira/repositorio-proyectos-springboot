package com.andrespr.springboot.form.app.editors;

import java.beans.PropertyEditorSupport;
//Clase para poner el nombre recibido por el formulario en mayus
//Debe heredar de PropertyEditorSupport
public class NombreMayusculaEditor extends PropertyEditorSupport{

	//click derechero/source/OverrideorImplementsMethod/setAsText
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		setValue(text.toUpperCase().trim());
	}

	
}
