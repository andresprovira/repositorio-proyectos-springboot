package com.andrespr.springboot.form.app.services;

import java.util.Arrays;

import java.util.List;

import org.springframework.stereotype.Service;

import com.andrespr.springboot.form.app.models.Pais;

@Service
public class PaisServiceImpl implements PaisService {

	private List<Pais> lista;
	
	public PaisServiceImpl() {
	
		this.lista= Arrays.asList(new Pais(1,"ES","España"),
				new Pais(2,"MX","México"),
				new Pais(3,"CL","Chile"),
				new Pais(4,"AR","Argentinca"),
				new Pais(5,"PE","Peru"),
				new Pais(6,"CO","Colombia"));
	}

	@Override
	public List<Pais> lista() {
		
		return lista;
	}

	@Override
	public Pais obtenerPorId(Integer id) {
		Pais resultado = null;
		
		for(Pais pais : lista) {
			if(pais.getId()==id) {
				resultado=pais;
				break;
			}
			
		}
		return resultado;
	}

}
