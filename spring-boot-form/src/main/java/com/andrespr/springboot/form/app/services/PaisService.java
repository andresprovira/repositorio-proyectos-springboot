package com.andrespr.springboot.form.app.services;

import java.util.List;

import com.andrespr.springboot.form.app.models.Pais;

public interface PaisService {

	public List<Pais> lista();
	public Pais obtenerPorId(Integer id);
}
