package com.andrespr.springboot.form.app.controllers;



import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.andrespr.springboot.form.app.editors.NombreMayusculaEditor;
import com.andrespr.springboot.form.app.editors.PaisPropertiesEditor;
import com.andrespr.springboot.form.app.editors.RolePropertiesEditor;
import com.andrespr.springboot.form.app.models.Pais;
import com.andrespr.springboot.form.app.models.Role;
import com.andrespr.springboot.form.app.models.Usuario;
import com.andrespr.springboot.form.app.services.PaisService;
import com.andrespr.springboot.form.app.services.RoleService;
import com.andrespr.springboot.form.app.validations.UsuarioValidador;

@Controller
//Conseguimos que se mantenga la id
@SessionAttributes("usuario")
public class FormController {

	//Hay que hacer un autowired a un objeto del tipo validador que queremos validar
	@Autowired
	private UsuarioValidador usuValidador;
	//Forma de validar todos mis modelos con una clase de tipo validador
	//El problema que solo valida lo que haya configurado en UsuarioValidador, los campos del modelo no los valida
	
	@Autowired
	private PaisService paisService;
	//Necesitaremos el editor de pais para hacer la conversión
	
	@Autowired
	private RoleService roleService;
	
	
	//Properties y Editors
	@Autowired
	private PaisPropertiesEditor paisEditor;
	@Autowired
	private RolePropertiesEditor roleEditor;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		//Para solucionar el problema en vez de set, un add
		binder.addValidators(usuValidador);
		//Forma de validar una fecha como quiera, mejor de la forma de siempre
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false); //Recomendacion del curso, estricto al formato
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
		//Para que se ponga lo que queramos en mayus con el segundo parámetro especifico el campo que quiero 
		binder.registerCustomEditor(String.class,"nombre", new NombreMayusculaEditor());
		binder.registerCustomEditor(String.class,"apellido", new NombreMayusculaEditor());
		//Lo registramos en el binder--Tipo de dato, nombre del campo, editor
		binder.registerCustomEditor(Pais.class,"pais", paisEditor);
		binder.registerCustomEditor(Role.class, "roles", roleEditor);
	}
	
	@ModelAttribute("generos")
	public List<String> generos(){
		return Arrays.asList("Hombre","Mujer");
	}
	
	//Lista de paises de tipo String
	@ModelAttribute("paises")	
	public List<String> paises(){
		//Una forma de añadir elementos a una lista sin crear la lista y add
		return Arrays.asList("españa","francia","Mexico","Chile","Perú","Colombia");
	}
	
	//Para obtener una lista de paises como objetos
	@ModelAttribute("listaPaises")	
	public List<Pais> listaPaises(){
		//Una forma de añadir elementos a una lista sin crear la lista y add
		return paisService.lista();
			
	}
	
	//Lista de paises de tipo Map
		@ModelAttribute("paisesMap")	
		public Map<String,String> paisesMap(){
			//Forma de rellenar un selectList con un mapa
			Map<String,String> paises = new HashMap<String,String>();
			paises.put("ES", "España");
			paises.put("MX", "México");
			paises.put("CL", "Chile");
			paises.put("AR", "Argentina");
			paises.put("PE", "Perú");
			paises.put("CO", "Colombia");
		
			return paises;
		}
	
	//Lista de roles de tipo String
	@ModelAttribute("rolesString")
	public List<String> rolesString(){
		return Arrays.asList("ROLE_ADMIN","ROLE_USER","ROL_MODERATOR");
	}
	
	//Lista de roles de tipo Role
	@ModelAttribute("listaRoles")
	public List<Role> listaRoles(){
		return roleService.listar();
	}
	
	//Lista de roles de tipo Map<String,String>
	@ModelAttribute("rolesStringMap")	
	public Map<String,String> rolesStringMap(){
		//Forma de rellenar un selectList con un mapa
		Map<String,String> roles = new HashMap<String,String>();
		roles.put("ROLE_ADMIN", "Administrador");
		roles.put("ROLE_USER", "Usuario");
		roles.put("ROL_MODERATOR", "Moderador");
		
	
		return roles;
	}
	
	
			
	@GetMapping("/form")
	public String form(Model model) {
		Usuario usu = new Usuario();
		usu.setNombre("Jhon");
		usu.setApellido("Doe");
		usu.setId("13.123.123-G");
		usu.setHabilitar(true);
		usu.setValorSecreto("Algún valor secreto ****");
		
		//Ponemos esto para que en el formulario aparezca este valor establecido
		//En el caso de pais hay que hacer un toString de la id en el modelo
		usu.setPais(new Pais(3,"CL","Chile"));
		
		//El caso de Rol es mas complicado, ya que es una lista de objetos
		//Para ello tenemos que hacer un método equals en su modelo y luego en la vista un th:checked
		usu.setRoles(Arrays.asList(new Role(2,"Usuario","ROLE_USER")));
	
		
		model.addAttribute("titulo","Formulario de usuarios");
		model.addAttribute("usuario", usu);
		return "form";
	}
	
	@PostMapping("/form") //@Valid para validar los campos. Bindingresult objeto propio de spring, refleja el resultado de la validacion
															//Siempre tiene que ir despues del objeto que queremos validar sino no funciona
	public String procesar(@Valid Usuario usu ,BindingResult result , Model model){
		//Se utiliza en el post, pasando el objeto y el binding
		//usuValidador.validate(usu, result);
		
		
		if(result.hasErrors()) {	
			model.addAttribute("titulo","resultado del formulario");
			/*
			//Una forma para rellenar un mapa con el key y value, es una forma manual
			Map<String,String> errores = new HashMap<>();
			//con result.getFieldErrors() obtenemos una lista de los error, lo recorremos y rellenamos nuestro mapa, con el campo y el mensaje de rror
			result.getFieldErrors().forEach(err ->{
				errores.put(err.getField(), "El campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()));
			});
			model.addAttribute("error", errores);*/
			
			
			return "form";
		}
		
		
	
		//Los metodos post deben siempre hacer un redirect a un action de mi controlador, para no hacer la carga dos veces a la base de datos
		return "redirect:/ver";
	}
	
	@GetMapping("/ver")//Con SessionAttribute() obtenemos el objeto guardado en sesion
										//Para que no de fallo, required false
	public String ver(@SessionAttribute(name="usuario",required = false) Usuario usu,Model model, SessionStatus status) {
		
		//Controlamos si es null(cuando relogueamos la pagina no hay usuario porque lo hemos borrado en sesion)
		//Si es null lo redigirimos por ejemplo al form
		if(usu==null) {
			return "redirect:/form";
		}
		model.addAttribute("titulo","resultado del formulario");
		status.setComplete();// se elimina el objeto usuario de la sesion
		return "resultado";
	}
}
