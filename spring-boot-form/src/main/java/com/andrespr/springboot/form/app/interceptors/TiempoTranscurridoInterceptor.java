package com.andrespr.springboot.form.app.interceptors;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

//Los interceptores vienen siendo como los filtros en .net, aunque además podemos obtener informacion prehandle y postHandle
//Handle se refiere a un método del controlador
@Component
public class TiempoTranscurridoInterceptor implements HandlerInterceptor {
	
	//constante tipo logger de slf4j, LoggerFactory.getLogger(claseinterceptora)
	private static final Logger logger = LoggerFactory.getLogger(TiempoTranscurridoInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		//ESto es para si quiero tratarlo en una misma ruta que sea get y post a la vez
		if(request.getMethod().equalsIgnoreCase("post")) { //Si el método es post, nos salimos 
			return true;
		}
		
		//Esto es un poco locura, no es necesario, solo para ver info en la consola
		if(handler instanceof HandlerMethod) {
			HandlerMethod metodo = (HandlerMethod) handler;
			logger.info("es un método del controlador: " + metodo.getMethod().getName());
		}
		
		logger.info("TiempoTranscurridoInterceptor: preHandle() entrando...");
		logger.info("Interceptando: " + handler);//Para saber el error
		long tiempoInicio = System.currentTimeMillis();//Devuelve el tiempo el long en milisegundos
		request.setAttribute("tiempoInicio",tiempoInicio);//Guardamos en el request
		
		Random random = new Random();
		Integer demora = random.nextInt(501);
		Thread.sleep(demora);//Simulamos un delay
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		
		//ESto es para si quiero tratarlo en una misma ruta que sea get y post a la vez
				if(request.getMethod().equalsIgnoreCase("post")) { //Si el método es post, nos salimos 
					return;//En un método void el return sirve para salirse, sin ejecutar los pasos siguientes
				}
				
		long tiempoInicio = (Long)request.getAttribute("tiempoInicio");//Obtenemos el tiempo de inicio que guardamos en el request
		long tiempoFin = System.currentTimeMillis(); //Obtenemos el tiempo actual en el postHandle
		long tiempoTranscurrido= tiempoFin - tiempoInicio;//Obtenemos tiempo transcurrido
		if(handler instanceof HandlerMethod && modelAndView != null) { //Validacion importante
			modelAndView.addObject("tiempoTranscurrido",tiempoTranscurrido);//lo añadimos al modelAndView
		}
		logger.info("TiempoTranscurrido:" + tiempoTranscurrido + " milisegundos");//Lo informamos por log
		logger.info("TiempoTranscurridoInterceptor: postHandle() saliendo...");
	}

	
}
