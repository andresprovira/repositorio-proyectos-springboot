package com.andrespr.springboot.form.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.andrespr.springboot.form.app.interceptors.TiempoTranscurridoInterceptor;

@Configuration //Clase de configuracion para los interceptores, debe implementar de WebMvcConfigures
public class MvcConfig implements WebMvcConfigurer {

	//Inyectamos la clase interceptore
	@Autowired
	private TiempoTranscurridoInterceptor tiempoTranscurridoInterceptor;
	
	//Implementamos metodo addInterceptors y agregamos en registry nuestro interceptor
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(tiempoTranscurridoInterceptor).addPathPatterns("/form/**");//Si lo dejamos asi se aplicara a todas las rutas de mi controlador
																//Para cambiarlo .addPathPatterns("/ruta","ruta2"...)
	}

	
}
