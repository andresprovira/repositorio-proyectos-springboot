package com.andrespr.springboot.form.app.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.andrespr.springboot.form.app.services.PaisService;

//Creamos el editor(convertidos) que pasará la id en String a un objeto Pais, através de PaisService
@Component
public class PaisPropertiesEditor extends PropertyEditorSupport {

	@Autowired
	private PaisService service;
	@Override
	public void setAsText(String idString) throws IllegalArgumentException {
		//Parseamos la id a Integer
	
			try {
					Integer id = Integer.parseInt(idString);
		//Devolvemos el pais por la id
						this.setValue(service.obtenerPorId(id));
			}catch(NumberFormatException e){
				setValue(null);
			}
		
	}

	
}
