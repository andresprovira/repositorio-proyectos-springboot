package com.andrespr.springboot.horario.app.interceptor;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component("horario")
public class HorarioInterceptor implements HandlerInterceptor {

	@Value("${config.horario.apertura}")
	private Integer apertura;
	@Value("${config.horario.cierre}")
	private Integer cierre;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		Calendar calendar = Calendar.getInstance(); //Nos devuelve la fecha actual
		int hora = calendar.get(Calendar.HOUR_OF_DAY);//Devuelve la hora del día
		
		if(hora >=apertura && hora <cierre) {
			StringBuilder mensaje = new StringBuilder("Bienvenidos al horario de atencion al cliente");//Clase que es como un string y le puedes añadir mas String
			mensaje.append(", atendemos desde las ");//Con .append agreamos elementos a nuestro StringBuilder
			mensaje.append(apertura);
			mensaje.append("hrs. ");
			mensaje.append("hasta las ");
			mensaje.append(cierre);
			mensaje.append("hrs. ");
			mensaje.append("Gracias por su visita.");
			request.setAttribute("mensaje", mensaje.toString());
			return true;
		}
		response.sendRedirect(request.getContextPath().concat("/cerrado")); //Que hace nuestro método preHandle si devuelve false
																			//Esto es un redirect a la ruta que queramos
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	
		String mensaje = request.getAttribute("mensaje").toString();//Capturamos en el post con el request el mensaje enviado
		if(modelAndView!=null && handler instanceof HandlerMethod) { //Importante para evitar el error en la consola
		modelAndView.addObject("horario",mensaje);
		}
	}

	
}
