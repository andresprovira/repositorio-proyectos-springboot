package com.andrespr.springboot.web.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/params")
public class EjemploParamsController {

	
	@RequestMapping("/")
	public String index() {
		return "params/index";
	}
	
	//El RequestParam es para enviar un parámetro a la url
	@RequestMapping("/string")
	public String param(@RequestParam(required = false, defaultValue = "eeeey esto funciona") String texto,Model model) {
		
		model.addAttribute("resultado","el texto enviado es: "  + texto);
		return "params/ver";
	}
	
	@RequestMapping("/mix-params")
	public String param(@RequestParam String saludo,@RequestParam Integer num,Model model) {
		
		model.addAttribute("resultado","el saludo enviado es: "  + saludo + "y el número es " + "= " + num);
		return "params/ver";
	}
	//Con HttpdServletRequest recogemos datos de la url
	@RequestMapping("/mix-params-request")
	public String param(HttpServletRequest request,Model model) {
		
		String  saludo = request.getParameter("saludo");
		Integer num=null;
		try {
			 num = Integer.parseInt(request.getParameter("num"));
		} catch(NumberFormatException e) {
			
		}
		
		model.addAttribute("resultado","el saludo enviado es: "  + saludo + "y el número es " + "= " + num);
		return "params/ver";
	}
	
	
}
