package com.andrespr.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
		return "forward:/app/index";
		//Otra forma
		//return "forward:/app/index"
		//Con el redirect reinicia la página perdiendo los parámetros de http
		//Con forward no los pierde, interesante para el proyecto, solo sirve para rutas del controllador
	}
}
