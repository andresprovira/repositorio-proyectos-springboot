package com.andrespr.springboot.web.app.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.andrespr.springboot.web.app.models.Usuario;

@Controller
@RequestMapping({"/app"})
public class IndexController {
	
	
	//Con @Value puedo inyectar un valor que tenga establecido en application.properties
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;
	
	
	@RequestMapping(value ={"/index","/",""},method = RequestMethod.GET)
	public ModelAndView index(ModelAndView mv) {
		
		mv.addObject("titulo",textoIndex );
		mv.setViewName("index");
		return mv;
	}
	
	@RequestMapping("/perfil")
	public String perfil(Model model) {
		Usuario usu = new Usuario();
		usu.setName("Andrés");
		usu.setSurname("Pérez");
		usu.setEmail("micorreo@email.es");
		model.addAttribute("usuario", usu);
		model.addAttribute("titulo",textoPerfil  .concat(usu.getName()));
		return "perfil";
	}
	

	@RequestMapping("/listar")
	public String listar(Model model) {
		
		
		model.addAttribute("titulo",textoListar);
		
		
		return "listar";
	}
	
	//Con @ModelAttribute puedo realizar métodos para rellenar una lista y guardarlo en el model que servirá 
	//Para todo el controlador, muy útil para selectlist
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios(){
		
		
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario("pepe","martin","pepe@email.es"));
		usuarios.add(new Usuario("jose","mali","jose@email.es"));
		usuarios.add(new Usuario("maria","muñe","maria@email.es"));
		
		return usuarios;
	}
	
}
