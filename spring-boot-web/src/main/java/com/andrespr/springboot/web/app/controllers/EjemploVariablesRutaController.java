package com.andrespr.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploVariablesRutaController {

	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("titulo", "enviar parámetro de la ruta (@PathVariable)");
		return "variables/index";
	}
	
	@RequestMapping("/string/{texto}")
	public String variables(@PathVariable String texto, Model model) { //El nombre de la variable de Pathvariable debe ser igual que
														//El que viene por la url
	
		model.addAttribute("titulo", "Recibir parámetro de la ruta (@PathVariable)");
		model.addAttribute("resultado", "El texto enviado en la ruta es: " + texto);
		return "variables/ver";
	}
	
	@RequestMapping("/string/{texto}/{num}")
	public String variables(@PathVariable String texto,@PathVariable Integer num, Model model) { //El nombre de la variable de Pathvariable debe ser igual que
														//El que viene por la url
	
		model.addAttribute("titulo", "Recibir parámetro de la ruta (@PathVariable)");
		model.addAttribute("resultado", "El texto enviado en la ruta es: " + texto + " el numero es = " + num);
		return "variables/ver";
	}
}
